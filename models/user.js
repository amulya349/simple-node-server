// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var userSchema = new Schema({
  username: { type: String, required: true, unique: true },
  location: String,
  age: Number,
  email: String,
  created: String
});

// add the timestamp of creation of the entry
userSchema.pre('save', function(next) {
  // get the current date
  var currentTime = new Date().toString();
  this.created = currentTime;

  next();
});

// we need to create a model using it
var User = mongoose.model('User', userSchema);

// make this available to our Node applications
module.exports = User;