# simple-node-server

This is a simple node server using express and MongoDB

###Instructions to start the server
```sh
npm install 
node ./server.js
```

### User Schema: 
```js
{
  username: { type: String, required: true, unique: true },
  location: String,
  age: Number,
  email: String
}
```

### URL routes: 
- `/fetch?username=<username>`    [GET]
- `/create`    [POST]