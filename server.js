var app = require("express")();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var User = require('./models/user');
var port = process.argv[2] || 8000;

//setup code for bodyparser to use urlencoded as well as json for post methods
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

//startup of the server
app.listen(port, function(){
	console.log("Server is on at http://localhost:"+port);
})

//connect to the Mongo Database
mongoose.connect('mongodb://localhost/nodeapp');

app.get('/', function(req, res){
	res.send("Welcome to My Server. <br><br>1. Use \/create to create user by POST <br>2. Use fetch to \/fetch user data by giving the username by GET.");

})

// method to create user by POST
app.post('/create', function(req, res){

	//create a new User object
	var newUser = new User({
		username: req.body.username,
		location: req.body.location,
		age: req.body.age,
		email: req.body.email
	});

	//save the new created user to database
	newUser.save(function(err) {
	  if (err) throw err;

	  res.json({message: 'User created!'});
	});
})

//method to fetch user by GET
app.get('/fetch', function(req, res){

	User.find({ username: req.query.username }, function(err, user) {
	  if (err) throw err;

	  // object of the user
	  res.json(user);
	});
})